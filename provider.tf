terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
  }
}

variable "digitalocean_token" {}

provider "digitalocean" {
  token = "${var.digitalocean_token}"
}

terraform {
  backend "s3" {
    endpoint = "nyc3.digitaloceanspaces.com"
    bucket = "backend-space"
    key    = "terraform.tfstate"
    region = "us-east-1"
    skip_requesting_account_id = true
    skip_credentials_validation = true
    skip_get_ec2_platforms = true
    skip_metadata_api_check = true
  }
}
