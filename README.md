Use terraform para declarar la infraestructura en la que hay un cluster de Kubernetes en digitalocean.


Para manejar el estado de terraform utilice un servicio s3 Bucket donde almaceno el archivo terraform.tfstate para que no importa desde donde uso el servicio de terraform (o si lo usa el Job de la Pipeline) 

En la creación del cluster defino el nombre que le quiero poner al cluster mediante una variable de entorno para que sea mas facil el despliegue de otros entornos.

Si el cluster por algun motivo no esta corriendo, terraform iniciara el cluster y para desplegar hacia kubernetes, utilizo doctl para autenticarme con mi token de digital ocean y obtener la kubeconfig.

Variables de entorno necesarias

CI_REGISTRY_IMAGE
CI_REGISTRY_PASSWORD
CI_REGISTRY_USER
DIGITALOCEAN_API_KEY 
SPACES_ACCESS_TOKEN 
SPACES_SECRET_KEY 
TF_VAR_CLUSTER_NAME
TF_VAR_digitalocean_token

Comentarios personales: Fue un desafio que disfrute realizandolo ya aprendi mucho sobre los proveedores y herramientas que no habia tenido la oportunidad de trabajar con ellas.

Puede acceder al servicio:
http://165.227.255.221/

